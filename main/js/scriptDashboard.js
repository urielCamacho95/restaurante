function displayModal(){
  $('#modalAddSaucerFood').addClass("show");
  $('#modalAddSaucerFood').css("display","block");
}
function hideModal(){
  $('#modalAddSaucerFood').removeClass("show");
  $('#modalAddSaucerFood').css("display","none");
}
$(document).ready(function(){
  $('#addSaucerFood a').bind('click',displayModal);
  $('#closeModalSaucerFood').bind('click',hideModal);
  $('.modal-footer button').click(function(){
    $('#closeModalSaucerFood').trigger('click');
  });
});
